package utils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class DataUtils {
	
	
	
	public static Properties  loadPropertiesfile() throws IOException
	{
		
		FileReader DestFile=new FileReader(System.getProperty("user.dir") + "\\Resources\\");
		
		Properties properties=new Properties();
		properties.load(DestFile);
		
		return properties;
	}
	
	
	
	public static String getProperty(String key) throws IOException
	{
	
		return loadPropertiesfile().getProperty(getProperty(key));
	}
	
	
	

}
