package com.Prolution.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.ClickAction;
import org.openqa.selenium.interactions.DoubleClickAction;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

	protected ThreadLocal<WebDriver> driver = new ThreadLocal<>();
	PageCollection pages;

	public BasePage(ThreadLocal<WebDriver> driver) {
		this.driver = driver;
	}

	public WebElement waitforPresenceOFElement(By locator) {
		System.out.println();
		WebElement firstResult = new WebDriverWait(driver.get(), 60)
				.until(ExpectedConditions.presenceOfElementLocated(locator));
		return firstResult;

	}

	public WebElement waitforPresenceOFElementUntilCLickable(By locator) {
		// System.out.println();
		WebElement firstResult = new WebDriverWait(driver.get(), 60)
				.until(ExpectedConditions.elementToBeClickable(locator));

		return firstResult;

	}

	public void clickandwait(By locator) {
		WebElement el = waitforPresenceOFElementUntilCLickable(locator);
		el.click();
        
	}
	
	

	public void ClickUsingActionClass(By locator) {
		WebElement el = driver.get().findElement(locator);
		new Actions(driver.get()).click(el).build().perform();
		System.out.println("element has been clicked");
	}

	public void Clickandwait(By locator) {
		waitforelementtoBecomePresent(locator);
		waitforelementtoBecomeclickable(locator);
		driver.get().findElement(locator).click();
	}

	public void SeTText(By locator, String keysToSend) {
		WebElement el = waitforPresenceOFElement(locator);
		el.sendKeys(keysToSend);
	}

	public void switchTab(int i) {
		ArrayList<String> tabs = new ArrayList<String>(driver.get().getWindowHandles());

		driver.get().switchTo().window(tabs.get(i));
	}

	public void waitforelementtoBecomePresent(By locator) {
		WebDriverWait wait = new WebDriverWait(driver.get(), 60);
		WebElement el = wait.until(ExpectedConditions.presenceOfElementLocated(locator));

	}

	public void waitforelementtoBecomeclickable(By locator) {
		WebDriverWait wait = new WebDriverWait(driver.get(), 20);
		try {
			WebElement el = wait.until(ExpectedConditions.elementToBeClickable(locator));
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}

	}

	public boolean waitforelementtoBecomeVisible(By locator) {
		WebDriverWait wait = new WebDriverWait(driver.get(), 60);
		WebElement el = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		return el.isDisplayed();

	}
	public boolean waitforelementtoBecomeInVisible(By locator) {
		WebDriverWait wait = new WebDriverWait(driver.get(), 60);
		Boolean el = wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
		return el;

	}

	public void SetText(By locator, String text) {
		waitforelementtoBecomePresent(locator);
		waitforelementtoBecomeclickable(locator);
		driver.get().findElement(locator).clear();
		driver.get().findElement(locator).sendKeys(text);
	}

	public void handleAlert() {
		if (isAlertPresent()) {
			Alert alert = driver.get().switchTo().alert();
			System.out.println("Alert Text is" + alert.getText());
			alert.accept();
		}
	}

	/**
	 * Checks if is alert present.
	 *
	 * @return True if JavaScript Alert is present on the page otherwise false
	 */
	public boolean isAlertPresent() {
		try {
			driver.get().switchTo().alert();
			return true;
		} catch (NoAlertPresentException ex) {
			return false;
		}
	}

	public void SelectFromDropDown(By element, String NameOfOption) {

		waitforelementtoBecomeclickable(element);
		Select sel = new Select(driver.get().findElement(element));
		// sel.selectByValue(NameOfOption);
		sel.selectByIndex(0);

	}
	
	public void SelectFromDropDownUsingNameOfOption(By element, String NameOfOption) {

		waitforelementtoBecomeclickable(element);
		Select sel = new Select(driver.get().findElement(element));
		// sel.selectByValue(NameOfOption);
		sel.selectByValue(NameOfOption);

	}

	public void SelectFromDropDownUsingIndex(By element, int Index) {

		waitforelementtoBecomeclickable(element);
		Select sel = new Select(driver.get().findElement(element));
		// sel.selectByValue(NameOfOption);
		sel.selectByIndex(Index);

	}

	public void PerFormActionUsingActionsClass(Consumer<Actions> consumer) {

		consumer.accept(new Actions(driver.get()));
	}

	public String getAttribute(By locator, String NameOfAttribute) {
		return driver.get().findElement(locator).getAttribute(NameOfAttribute);

	}

	public boolean IsElementPresent(By locator) {
		return driver.get().findElement(locator).isDisplayed();
	}

	public List<WebElement> ReturnListOfElements(By locator) {
		return driver.get().findElements(locator);
	}

	public void PressEnter(By locator) {

		driver.get().findElement(locator).sendKeys(Keys.RETURN);
	}

	public void PressEnter() {

		Actions act=new Actions(driver.get());
		act.sendKeys(Keys.RETURN).build().perform();
	 
	}
	
	public void PressEnterByUsingAlert() 
	{
		Alert al=driver.get().switchTo().alert();
		al.accept();
	}
	
	public final void javascriptButtonClick(By webElement) {
		WebElement el = driver.get().findElement(webElement);
		JavascriptExecutor js = (JavascriptExecutor) driver.get();
		js.executeScript("arguments[0].click();", el);
		System.out.println("javascript button has been clicked.");
	}

	public void MultipleCheckBoxSelector(By Buttonlocator, By optionLocator) {

		waitforelementtoBecomeVisible(Buttonlocator);
		clickandwait(Buttonlocator);
		// waitforelementtoBecomeclickable(locator);

		waitforelementtoBecomeVisible(optionLocator);
		clickandwait(optionLocator);

	}

	public void DoubleClick(By locator) {

		WebElement el = driver.get().findElement(locator);
		new Actions(driver.get()).doubleClick(el).build().perform();
	}
	
	public void InputFiles(By locator,String PathName) 
	{
	   WebElement chooseFile=driver.get().findElement(locator);
	   chooseFile.sendKeys(PathName);
	}
	
	
	public final void waitTillAlertPresent() {
		try {

			WebDriverWait wait = new WebDriverWait(driver.get(), 60);
			wait.until(ExpectedConditions.alertIsPresent());

			Alert alert = driver.get().switchTo().alert();
			alert.accept();

		} catch (NoAlertPresentException noAlert) {
			noAlert.getMessage();
		}
	}

}
