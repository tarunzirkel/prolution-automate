package com.Prolution.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.TestUtils;

public class CustomersPage extends BasePage {

	public CustomersPage(ThreadLocal<WebDriver> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	By CustomerButton = By.id("customers-nav");

	By AddCustomersButton = By.xpath("//div[@class='col-md-5 page-title-adj btn-bar text-right']//a[@href='/customers/new']");
	
	
	By SearchField=By.xpath("//*[@class='twitter-typeahead']//input");


	By SearchButton=By.xpath("//*[@class='search-action-btns']//input");
	
	By AllEmailfield = By.xpath("//b[text()='Email']//following-sibling::a");

	By SuccessMessage = By.xpath("//button[@class='close']//parent::div[@class='alert alert-info']");

	By NextArrowParentElement = By.xpath("//ul[@class='pagination pagination']//li[4]");
	
	By NextArrow=By.className("next");
	
	By EditButton=By.xpath("//i[contains(@class,'pencil')]//parent::a");
	

	public void clickOnCustomersButton() {
		clickandwait(CustomerButton);

	}

	public void ClickOnAddCustomersButton() {
		clickandwait(AddCustomersButton);

	}

	public void ClickOnEditButton()
	{
		clickandwait(EditButton);
	}
	
	
	
	
	
	
	
	public void waitforTableToAppear() {
		WebDriverWait wait = new WebDriverWait(driver.get(), 60);
		wait.withMessage("waiting for the customer table to appear")
				.until(ExpectedConditions.numberOfElementsToBeMoreThan(AllEmailfield, 1));

	}

	public boolean VerifyEmailOfNewlyCreatedCustomer(String Email) {
		System.out.println("In email verification method");
		List<WebElement> li = driver.get().findElements(AllEmailfield);
		li.forEach(l->System.out.println(l.getText().equalsIgnoreCase(Email)));
		return li.stream().anyMatch(l -> (l.getText()).trim().equalsIgnoreCase(Email.trim()));

	}

	public boolean VerifyEmailOnCustomerPage(String Email) {
		int i=1;
		while(i>0) {
			waitforTableToAppear();
			boolean status=VerifyEmailOfNewlyCreatedCustomer(Email);
			if(status)
			{
				return status;
			}
			
			ClickUsingActionClass(NextArrow);
			if(getAttribute(NextArrowParentElement, "class").contains("disabled"))
			{i=0;}
			
		} 
		return false;
	}

	public boolean verifySucessMessage() {
		return waitforelementtoBecomeVisible(SuccessMessage);

	}
	
	
	
	
	
	

	public void SearchForCustomer(String Name_Email_Phone)
	{
		SetText(SearchField, Name_Email_Phone);
		ClickOnSearchButton();
		
	}
	
	public void ClickOnSearchButton()
	{
		clickandwait(SearchButton);
	}
	
	
	
	public void SelectAnOptionFromActionsDropdownForCustomer(String optionName)
	{
		
		
		waitforelementtoBecomeVisible(By.xpath("//ul[@class='dropdown-menu']//li[1]//input"));
		List<WebElement>options=driver.get().findElements(By.xpath("//div[contains(@class,'open')]//ul[@class='dropdown-menu']//li//input[not(@type='hidden')]"));
		
		if(optionName.startsWith("Mer"))
		{
			options.get(0).click();
			return;
		}
		if(optionName.startsWith("Arc"))
		{
			options.get(1).click();
			return ;
			
		}
		
		if(optionName.startsWith("Del"))
		{
			options.get(2).click();
			return ;

	}
	}
	
	
	
}
