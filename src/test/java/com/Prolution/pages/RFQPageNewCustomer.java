package com.Prolution.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RFQPageNewCustomer extends BasePage {

	public RFQPageNewCustomer(ThreadLocal<WebDriver> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	
	By NewCustomerButton = By.xpath("//span[@id='newCustomerButton']//img");
	
	By CompanyName=By.xpath("//input[@class='form-control'][@id='COMPANY']");
	
	By Location = By.xpath("//input[@class='form-control'][@id='Location']");
	
	By Country =By.xpath("//Select[@class='select2Selection firstTabInput']");
	
	By DesiredCountry=By.xpath("//Select[@class='select2Selection firstTabInput']//option[@value='India']");
	
	By SAPId=By.xpath("//input[@class='form-control'] [@id='SAP_ID']");
	
	
	By CustomerChecked=By.xpath("//input[@type='checkbox'] [@id='ISCustomercheck']");
	
	
	By SaveButton=By.xpath("//button[@id='saveCustomer']");
	
	
	
	public void ClickOnNewCustomerButton()
	{
		waitforelementtoBecomePresent(NewCustomerButton);
		javascriptButtonClick(NewCustomerButton);
	}

	
	public void EnterCompanyName(String NameOfCompany) {
		SetText(CompanyName, NameOfCompany);
	}
	
	
	public void EnterLocation(String NameOfLocation)
	{
		SetText(Location, NameOfLocation);
	}
	
	
	public void SelectCountry() 
	{
		clickandwait(Country);
		waitforelementtoBecomeVisible(DesiredCountry);
		clickandwait(DesiredCountry);	
	}
	
	
	public void EnterSAPId(String Id)
	{
		SetText(SAPId, Id);
	}
	
	
	public void ClickOnCheckBox()
	{
		clickandwait(CustomerChecked);
	}
	
	
	//public void Click
	
}
