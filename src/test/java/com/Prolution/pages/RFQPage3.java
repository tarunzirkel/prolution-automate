package com.Prolution.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.ClickAction;

public class RFQPage3 extends BasePage {

	public RFQPage3(ThreadLocal<WebDriver> driver) {
		super(driver);

	}

	By FirstFolder = By.xpath("//div[@id='jstree_folders']/ul[1]/li/a[1]");      //Top Folder

	By KundendatenFolder = By.xpath("//*[@foldername='Kundendaten']");

	By AnfrageunterlagenFolder = By.xpath("//a[@foldername='Kundendaten']//parent::li//ul//li[1]//a");
	
	By Zeichnungen=By.xpath("//*[@foldername='Zeichnungen']");

	By SelectFiles = By.xpath("//input[@id='upload_area_inp']");

	By UploadButton = By.xpath("//button[@id='uploadFilesButton']");
	
	By CheckForFileUpdate=By.xpath("//input[@class=' move-files-checkbox']");
	
	By RadioButton=By.xpath("//input[@id='SelectMaterialRadio2']");                          //Select Second Material Through Radio Button

	By waitFor=By.xpath("//input[@id='selectAllFilesCheckbox']");
	
	By NextButton = By.xpath("//button[text()='Next']");

	public void ClickOnTheFolders() {
		waitforelementtoBecomeclickable(FirstFolder);
		clickandwait(FirstFolder);

	}

	public void ClickOnSubFolder() {
		waitforelementtoBecomeclickable(KundendatenFolder);
		DoubleClick(KundendatenFolder);
	}

	public void ClickOnChildFolderOfSubFolder() {
		waitforelementtoBecomePresent(AnfrageunterlagenFolder);
		clickandwait(AnfrageunterlagenFolder);

	}
	
	public void ClickOnChildFolderOfSubFolders() {
		waitforelementtoBecomePresent(Zeichnungen);
		clickandwait(Zeichnungen);

	}

		
	public void SelectFile() {
		waitforelementtoBecomePresent(SelectFiles);                                                  //Select File for Aluminium
		InputFiles(SelectFiles, "C:/Users/Bacancy/git/CI_CD1/Resources/Files/LESSON9.pdf");
		
	}

	public void SelectFiles() {
		waitforelementtoBecomePresent(SelectFiles);                                                  //Select File for Iron
		InputFiles(SelectFiles, "C:\\Users\\Bacancy\\git\\CI_CD1\\Resources\\Files\\INVOICE-template.docx");
		
	}
	
	public void ClickOnUploadButton() {
		waitforelementtoBecomeclickable(UploadButton);
		clickandwait(UploadButton);
		waitforelementtoBecomeclickable(CheckForFileUpdate);
	}
	

	public void ClickOnNextButton() {
		waitforPresenceOFElementUntilCLickable(NextButton);
		ClickUsingActionClass(NextButton);
	}
	
	public void ClickOnRadioButton() {
		waitforPresenceOFElement(RadioButton);
		javascriptButtonClick(RadioButton);
	}
	
	

}
