package com.Prolution.pages;

import java.util.List;
import java.util.function.Consumer;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

/**
 * @author Bacancy
 *
 */
public class InvoicesPage extends BasePage{

	public InvoicesPage(ThreadLocal<WebDriver> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	
	By CreateNewInvoicesButton=By.xpath("//*[@id='bulk_action_button']//preceding-sibling::a");
	
	
	By CustomerNAmeInputField=By.xpath("//label//following-sibling::input");
	
	By CreateNewInvoiceButton=By.xpath("//*[@class='btn btn-teal btn-sm']");
	
	By NewInvoiceButton=By.xpath("//div[@class='form-actions']//input");
	By FirstCustomerOption=By.xpath("//*[@id='ui-id-1']//li//div[1]");
	
	By FirstCustomerIdOnInvoiceTable=By.xpath("//tbody//tr[1]//td/a/span");
	
	By CustomerNameOnInvoice=By.xpath("//tbody//tr[1]//td[3]//a");
	
	By InvoicesButton=By.xpath("//ul[@class='mainnav']/li[2]");
	
	By InvoiceNumbergeneratedOnBillingPage=By.tagName("h1");
	
	By InvoiceSearchField=By.id("search_common");
	
	By SearchButton=By.xpath("//span[@class='input-group-btn']//button");
	
	By ActionsButton=By.xpath("//a[contains(text(),'Actions')]");
	
	By TookPayment=By.id("invoice_took_payment");
	
	
	By DatePaid=By.id("invoice_date_received_label");
	
	By todayDate=By.xpath("//td[contains(@class,'ui-datepicker-today')]//a");
	
	By updateInvoice=By.xpath("//input[contains(@value,'Update')]");
	
	By DeleteInfoMessage=By.xpath("//div[contains(@class,'alert-info')]");
	
	
	public void clickOnFirstInvoiceOption()
	{
		waitforelementtoBecomeVisible(FirstCustomerOption);
		clickandwait(FirstCustomerOption);
	}
	
	
	public void ClickOnCreateNewInvoiceButton()
	{
		waitforelementtoBecomeclickable(CreateNewInvoiceButton);
		clickandwait(CreateNewInvoiceButton);
		
	}
	
	public void ClickOnCreateInvoiceButton()
	{
		waitforelementtoBecomeclickable(NewInvoiceButton);
		clickandwait(NewInvoiceButton);
		
	}
	
	public String GetFirstInvoiceIdOnCustomerTable()
	{
	waitforelementtoBecomePresent(FirstCustomerIdOnInvoiceTable);	
	return driver.get().findElement(FirstCustomerIdOnInvoiceTable).getText();
	}

	
	public void ClickOnFirstInvoiceIdOnCustomerTable()
	{
		waitforelementtoBecomeVisible(FirstCustomerIdOnInvoiceTable);

		clickandwait(FirstCustomerIdOnInvoiceTable);
		
	}
	
	
	public void SelectAnOptionFromActions(String optionName)
	{
		
		ClickOnActionButton();
		waitforelementtoBecomeVisible(By.xpath("//ul[@class='dropdown-menu ']//li[1]"));
		List<WebElement>options=driver.get().findElements(By.xpath("//ul[@class='dropdown-menu ']//li"));
		
		if(optionName.startsWith("Quick"))
		{
			options.get(0).click();
			return;
		}
		if(optionName.startsWith("Ad-"))
		{
			options.get(1).click();
			return ;
			
		}
		
		if(optionName.startsWith("Add"))
		{
			options.get(2).click();
			return ;
			
		}
		if(optionName.startsWith("Make"))
		{
			options.get(3).click();
			return ;
			
		}
		
		if(optionName.startsWith("Clone"))
		{
			options.get(4).click();
			return ;
			
		}
		if(optionName.startsWith("Del"))
		{
			options.get(5).click();
			return ;
			
		}
	}
	
	
	public void ClickOnActionButton()
	{
		waitforelementtoBecomeVisible(ActionsButton);
		clickandwait(ActionsButton);
		
	}
	
	public String GetFirstCustomerNameOnCustomerTable()
	{
	waitforelementtoBecomePresent(CustomerNameOnInvoice);	
	return driver.get().findElement(CustomerNameOnInvoice).getText();
	}


	public void ClickOnInvoicesButton()
	{
		
		waitforelementtoBecomeclickable(InvoicesButton);
		clickandwait(InvoicesButton);
	}

	
	public void ClickOnNewInvoiceBUtton()
	{
		clickandwait(CreateNewInvoiceButton);
		
	}
	
	
	
	public void SetCustomerNameOnInvoiceField(String NameOfCustomer)
	{
		SeTText(CustomerNAmeInputField, NameOfCustomer);
		
	}
	
	
	
	
	public void SelectFirstCustomerNameFromCustomerDropdown()
	{
		waitforelementtoBecomeVisible(FirstCustomerOption);
		clickandwait(FirstCustomerOption);
		
	}
	
	
	
	public String getInvoiceNumberFromBillingPage()
	{
		waitforelementtoBecomeVisible(InvoiceNumbergeneratedOnBillingPage);
		return driver.get().findElement(InvoiceNumbergeneratedOnBillingPage).getText();
	}
	
	
	public void SearchInvoice(String InvoiceNumber) 
	{
		SeTText(InvoiceSearchField, InvoiceNumber);
		
		
	}
	
	
	public void CLickOnSearchButton()
	{
		
		clickandwait(SearchButton);
		
		
	}
	
	
	
	public void clickOnTookPaymentCheckbox(String status)
	{
		clickandwait(TookPayment);
	}

	
	public void clickOnTodayDate()
	{
		clickandwait(DatePaid);
		clickandwait(todayDate);
	}
	
	public void ClickOnUpdateInvoiceButton()
	{
		clickandwait(updateInvoice); 
	}
	
	public boolean VerifyDeleteSuccessMessage()
	{
		waitforelementtoBecomeVisible(DeleteInfoMessage);
		return IsElementPresent(DeleteInfoMessage);
		
	}
	
	
	
	
	
	
	
	
	
	
}
