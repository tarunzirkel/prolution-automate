package com.Prolution.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Extras.MainMenuItem;

public class HomePage extends BasePage {

	public HomePage(ThreadLocal<WebDriver> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
	By MainMenuItemList=By.xpath("//ul[@class='mainnav']/li");
	
	By MoreButtonItemList=By.xpath("//ul[@class='dropdown-menu' and @role='menu']/li");
	
	public enum More { Invoices,Tickets,Estimates }  
	public void ClickOnAMainMenuItem(MainMenuItem ButtonName)
	{
		waitforelementtoBecomeVisible(MainMenuItemList);
		List<WebElement>el=ReturnListOfElements(MainMenuItemList);
	
		
		if(ButtonName==MainMenuItem.More)
		{
			el.get(22).click();
		}
		if(ButtonName==MainMenuItem.Customers)
		{
			el.get(4).click();
		}
		
		
	}
	
	
	public void ClickOnItemUnderMainButton(More button)
	{
		waitforelementtoBecomePresent(MoreButtonItemList);
		List<WebElement>el=ReturnListOfElements(MoreButtonItemList);
		
		if(button==More.Invoices)
		{
		el.get(9).click();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
