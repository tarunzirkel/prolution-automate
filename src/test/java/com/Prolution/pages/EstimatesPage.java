package com.Prolution.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EstimatesPage extends BasePage{

	public EstimatesPage(ThreadLocal<WebDriver> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	
	By EstimatesButton = By.xpath("//ul[@class='mainnav']/li[3]");

	By AddEstimatesButton=By.xpath("//a[contains(@class,'btn-teal')]");
	
	
	By CustomersNameField=By.xpath("//label//following-sibling::input");
	
	By EditButton=By.xpath("//a[text()='Edit']");
	
	By ActionsButton=By.xpath("//a[contains(text(),'Actions')]");
	
	public void clickOnEstimateButton() {
		clickandwait(EstimatesButton);
	}
	
	
	
	public void ClickOnAddNewEstimate()
	{
		clickandwait(AddEstimatesButton);
	}
	
	
	
	public void SetCustomerNameOnEstimateField(String NameOfCustomer)
	{
		SeTText(CustomersNameField, NameOfCustomer);
		
	}
	
	
	public void ClickOnCreateEstimate()
	{
		clickandwait(AddEstimatesButton);
	}
	
	
	public void ClickOnEditButton()
	{
		clickandwait(EditButton);
	}
	
	public void ClickOnActionButton()
	{
		waitforelementtoBecomeVisible(ActionsButton);
		clickandwait(ActionsButton);
		
	}
	public void SelectAnOptionFromActions(String optionName)
	{
		
		ClickOnActionButton();
		waitforelementtoBecomeVisible(By.xpath("//div[contains(@class,'open')]//ul[@class='dropdown-menu ']//li[1]"));
		List<WebElement>options=driver.get().findElements(By.xpath("//div[contains(@class,'open')]//ul[@class='dropdown-menu ']//li"));
		
		
		if(optionName.startsWith("Ad-"))
		{
			options.get(0).click();
			return ;
			
		}
		
		if(optionName.startsWith("Add"))
		{
			options.get(1).click();
			return ;
			
		}
		if(optionName.startsWith("Clone"))
		{
			options.get(2).click();
			return ;
			
		}
		
		
		if(optionName.startsWith("Del"))
		{
			options.get(3).click();
			return ;
			
		}
	}
	
	
}
