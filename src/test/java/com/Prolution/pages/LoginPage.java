package com.Prolution.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage {

	public LoginPage(ThreadLocal<WebDriver> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	By UserName=By.id("username");
	
	By Passwordfield=By.id("password");
	
	By LoginButton=By.id("loginButton");
	
	By MainMenu=By.id("main-menu");
	
	By RFQTab=By.id("RFQ");
	
	By CreateRFQ=By.xpath("//li[@id='create-rfq']//a");
	By CustomerDropdown=By.id("ORGUNIT");
	
	
	public void getURL(String URL)
	{
		driver.get().get(URL);
		waitforPresenceOFElement(UserName);
	}
	
	
	public void SetEmailAndPassword(String Phone,String Password)
	{
		SeTText(UserName, Phone);
		SeTText(Passwordfield, Password);
		
	}
	
	public void ClickOnLoginButton() throws InterruptedException
	{
		clickandwait(LoginButton);
		waitforelementtoBecomePresent(MainMenu);
		
	}
	
	
	public void MoveToRFQAndClick()
	{
		Actions act=new Actions(driver.get());
		clickandwait(RFQTab);
		waitforelementtoBecomeVisible(CreateRFQ);
		clickandwait(CreateRFQ);
		waitforelementtoBecomePresent(CustomerDropdown);
		
		
		
	}
	
	
	
	
	
	
	
	
	

}