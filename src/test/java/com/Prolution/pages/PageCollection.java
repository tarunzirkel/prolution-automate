package com.Prolution.pages;

import org.openqa.selenium.WebDriver;

public class PageCollection {

	ThreadLocal<WebDriver>driver;
	public PageCollection(ThreadLocal<WebDriver> driver) {
		this.driver = driver;
	}

	private LoginPage loginPage;

	private CustomersPage homePage;
	
	private NewCustomerPage newCustomerPage;
	
	private InvoicesPage invoicesPage;
	
	private EstimatesPage estimatesPage;
	
	private HomePage home;
	
	private RFQPage1 rFQPage1;
	
	
	private RFQPage2 rFQPage2;
	private RFQPage3 rFQPage3;
	private RFQPage4 rFQPage4;
	private RFQPageNewCustomer rFQPageNewCustomer;

	public LoginPage getLoginPage() {

		return (loginPage == null) ? loginPage = new LoginPage(driver) : loginPage;
	}

	public CustomersPage getCustomersPage() {

		return (homePage == null) ? homePage = new CustomersPage(driver) : homePage;
	}
	
	public NewCustomerPage getNewCustomerPage() {

		return (newCustomerPage == null) ? newCustomerPage = new NewCustomerPage(driver) : newCustomerPage;
	}

	public RFQPage1 getCreateRFQPage1() {

		return (rFQPage1 == null) ? rFQPage1 = new RFQPage1(driver) : rFQPage1;
	}
	
	
	public RFQPage2 getRFQPage2() {

		return (rFQPage2 == null) ? rFQPage2 = new RFQPage2(driver) : rFQPage2;
	}
	
	public RFQPage3 getRFQPage3() {

		return (rFQPage3 == null) ? rFQPage3 = new RFQPage3(driver) : rFQPage3;
	}
	
	public RFQPage4 getRFQPage4() {

		return (rFQPage4 == null) ? rFQPage4 = new RFQPage4(driver) : rFQPage4;
	}
	
	
	public RFQPageNewCustomer getRFQPageNewCustomer() {

		return (rFQPageNewCustomer == null) ? rFQPageNewCustomer = new RFQPageNewCustomer(driver) : rFQPageNewCustomer;
	}
	
	
		
	public HomePage getHomePage() {

		return (home == null) ? home = new HomePage(driver) : home;
	}
	
	

	
	
	
	
}