package com.Prolution.pages;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RFQPage2 extends BasePage {

	public RFQPage2(ThreadLocal<WebDriver> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	By Nests=By.xpath("//input[@id=\'PLANT_CAVITIES\']");
	By RadioButton=By.xpath("//*[@id='defaultRadio'][@value='8104489d-3470-482b-be45-a594b767a330_FE']");    //fe
	By RadioButtonAL=By.xpath("//input[@id='defaultRadio'][@value='21b7927c-1f1e-4733-8b50-646da82ca472_AL']");    //al
	By PartNameField = By.id("PART_NAME");
	By RequestedPartButton = By.xpath("//select[@id='MATERIAL_TYPE']//following-sibling::div/button");

	By RequestPartOption = By.xpath("//select[@id='MATERIAL_TYPE']//following-sibling::div/ul//li[1]");

	By DrawingNumberRawCastingField = By.id("DRAWING_NUMBER_");

	By DrawingNumberZSB1 = By.id("DRAWING_NUMBER_ZSB");

	By DrawingIndexFinishedPart = By.xpath("//input[@id='DRAWING_INDEX_FP']");

	By DrawingDateRawCasting = By.xpath("//input[@id='DRAWING_DATE']");

	By DrawingDateZSB = By.xpath("//input[@id='DRAWING_DATE_ZSB']");

	By FinishedPartWeight = By.xpath("//input[@id='FINISHED_PART_WEIGHT']");

	By ReferenceId = By.xpath("//input[@id='REFERENCE_ID']");

	By Machine = By.xpath("//select[@id='MACHINE']");
	By MachineDropDownList = By.xpath("//*[@id=\"MACHINE\"]/option[2]");

	By PlantSelection = By.xpath("//select[@id='PLANT_SELECTION']");
	By PlantSectionDropDownList = By
			.xpath("//select[@id='PLANT_SELECTION']//option[text()='   Additive Manufacturing']");

	By PartNumberRawCasting = By.xpath("//input[@id='PART_NUMBER_']");

	By ZSB_Nr = By.xpath("//input[@id='ZSBNr']");

	By DrawingnumberFinishedPart = By.xpath("//input[@id='DRAWING_NUMBER_FP']");

	By DrawingIndexRawCasting = By.xpath("//input[@id='DRAWING_INDEX_']");

	By DrawingIndexZSB = By.xpath("//input[@id='DRAWING_INDEX_ZSB']");

	By DrawingDateFinishedPart = By.xpath("//input[@id='DRAWING_DATE_FP']");

	By RawCastingWeight = By.xpath("//input[@id='RAW_PART_WEIGHT']");

	By SAPMaterialNumber = By.xpath("//input[@id='SAP_MATERIAL_NR']");

	By Material_Alloy = By.xpath("//select[@id='MATERIAL_ALLOY_CODE']");
	By Material_AlloyDropDown = By.xpath("//*[@id=\"MATERIAL_ALLOY_CODE\"]/option[3]");

	By Volume_pieces_perYear = By.id("VOLUME_PA_");

	By Remarks = By.xpath("//textarea[@id='COMMENT']");

	By LifetimeModelEquipmentParts = By.xpath("//input[@id='COLUMN1']");

	By LifetimecoreBoxParts = By.xpath("//input[@id='COLUMN3']");

	By LifetimeStampingToolParts = By.xpath("//input[@id='COLUMN5']");

	By DataStatusFinishedPart = By.xpath("//input[@id='COLUMN7']");

	By CalculatedPrice = By.xpath("//input[@id='CalculatedField']");

	By LifeTimeInsertMatrixPart = By.xpath("//input[@id='COLUMN2']");

	By LifeTimeDieCastingTool = By.xpath("//input[@id='COLUMN4']");

	By DataStatusRawCasting = By.xpath("//input[@id='COLUMN6']");

	By OfferStage = By.xpath("//input[@id='COLUMN8']");

	
	By RequestedPart = By.xpath("//*[@id=\"Formdata1\"]/div/div[3]/div/span/span");

	By RequestedPartCheckBOXForSelectFinish = By
			.xpath("//*[@id=\"Formdata1\"]/div/div[3]/div/span/span/div/ul/li[3]/a/label/input");

	By PartNumberFinishedPart = By.xpath("//input[@id='PART_NUMBER_FP']");

	By FirstTableRow = By.xpath("//div[@id='DivDbdata']//tbody//tr[1]");

	By TypeOfRequest = By.xpath("//select[@id='TYPE_OF_PROJECT']");

	By SavePosition = By.id("savePosition");

	By NextButton = By.xpath("//button[text()='Next']");

	By OKButtonOnPopup = By.xpath("//div[@id='positionCreate']//button");

	public void EnterPartNameField(String NameOfPart) {
		SetText(PartNameField, NameOfPart);
	}

//	public void SelectCheckBoxForRequestedPart(int indexofOption) 
//	{
//		By RequestedPart=By.xpath("//button[@title='Finished Part']//following-sibling::ul["+indexofOption+"]//li[4]//input");
//		MultipleCheckBoxSelector(RequestedPart, RequestedPart);
//	}

	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	public void SelectCheck() {
		clickandwait(RequestedPart);
		waitforelementtoBecomeVisible(RequestedPartCheckBOXForSelectFinish);
		clickandwait(RequestedPartCheckBOXForSelectFinish);
	}
	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	public void ToSelectRadioButton() {
		clickandwait(RadioButton);
	}
	
	public void ToSelectRadioButtonAluminium() {
		clickandwait(RadioButtonAL);
	}
	
	public void EnterTheValueOfPartNumberFinishedPart(String FinishedPart) {
		SetText(PartNumberFinishedPart, FinishedPart);
	}

	public void EnterDrawingNumberRawCastingField(String DrawingNumberRawCastingField) {
		SetText(this.DrawingNumberRawCastingField, DrawingNumberRawCastingField);
	}

	public void EnterDrawingNumberZSB1(String DrawingNumberZSB1) {
		SetText(this.DrawingNumberZSB1, DrawingNumberZSB1);
	}

	public void EnterTheDrawingIndexFinishedPart(String DrawingFinishedPart) {
		SetText(DrawingIndexFinishedPart, DrawingFinishedPart);
	}

	public void EnterTheDrawingDateRawCasting(String DrawingDate) {
		SetText(DrawingDateRawCasting, DrawingDate);
		PressEnter(DrawingDateRawCasting);
	}

	public void EnterTheDrawingDateZSB(String DrawingDateZsb) {
		SetText(DrawingDateZSB, DrawingDateZsb);
		PressEnter(DrawingDateZSB);
	}

	public void EnterTheFinishedPartWeight(String PartWeight) {
		SetText(FinishedPartWeight, "10");
	}

	public void EnterTheReferenceId(String ReferenceID) {
		SetText(ReferenceId, ReferenceID);
	}

	public void EnterTheMachine() {
		clickandwait(Machine);
		waitforelementtoBecomeVisible(MachineDropDownList);
		clickandwait(MachineDropDownList);
	}

	public void EnterThePlantSelection() {
		clickandwait(PlantSelection);
		waitforelementtoBecomeVisible(PlantSectionDropDownList);
		clickandwait(PlantSectionDropDownList);
	}
	
	public void EnterTheNests(String Cavities)
	{
		SetText(Nests, Cavities);
	}

 	public void SelectTypeOfRequest(int index) {
		SelectFromDropDownUsingIndex(TypeOfRequest, index);
	}

	public void EnterThePartNumberRawCasting(String PartNumberRaw) {
		SetText(PartNumberRawCasting, PartNumberRaw);
	}

	public void EnterTheZSB_Nr(String ZsbNr) {
		SetText(ZSB_Nr, ZsbNr);
	}

	public void EnterTheDrawingnumberFinishedPart(String DrawingFinishpart) {
		SetText(DrawingnumberFinishedPart, DrawingFinishpart);
	}

	public void EnterTheDrawingIndexRawCasting(String IndexRawCasting) {
		SetText(DrawingIndexRawCasting, IndexRawCasting);
	}

	public void EnterTheDrawingIndexZSB(String IndexZSB) {
		SetText(DrawingIndexZSB, IndexZSB);
	}

	public void EnterTheDrawingDateFinishedPart(String DateFinishedPart) {
		SetText(DrawingDateFinishedPart, DateFinishedPart);
	}

	public void EnterTheRawCastingWeight(String CastingWeight)

	{
		SetText(RawCastingWeight, CastingWeight);
	}

	public void EnterTheSAP_MaterialNr(String SAPMaterial) {
		SetText(SAPMaterialNumber, SAPMaterial);
	}

	public void EnterTheMaterialAlloy() {
		clickandwait(Material_Alloy);
		waitforelementtoBecomeVisible(Material_AlloyDropDown);
		clickandwait(Material_AlloyDropDown);
	}

	public void EnterVolume_pieces_perYear(String Volume_pieces_perYear) {
		SetText(this.Volume_pieces_perYear, Volume_pieces_perYear);
	}

	public void EnterTheRemarks(String remark) {
		SetText(Remarks, remark);
	}

	public void EnterTheLifetimeModelEquipmentParts(String Equipment) {
		SetText(LifetimeModelEquipmentParts, Equipment);
	}

	public void EnterTheLifetimeCoreBoxParts(String CoreBoxParts) {
		SetText(LifetimecoreBoxParts, CoreBoxParts);
	}

	public void EnterTheLifetimeStampingToolParts(String StampingToolParts) {
		SetText(LifetimeStampingToolParts, StampingToolParts);
	}

	public void EnterTheDataStatusFinishedPart(String FinishedPart) {
		SetText(DataStatusFinishedPart, FinishedPart);
	}

	public void EnterTheCalculatedPrice(String Price) {
		SetText(CalculatedPrice, Price);
	}

	public void EnterTheValueOfLifetimeDieCastingToolParts(String DieCastingTools) {
		SetText(LifeTimeDieCastingTool, DieCastingTools);
	}

	public void EnterTheLifeTimeInsertPart(String InsertPart) {
		SetText(LifeTimeInsertMatrixPart, InsertPart);
	}

	public void EnterTheDataStatusRawCasting(String RawCasting) {
		SetText(DataStatusRawCasting, RawCasting);
	}

	public void EnterTheOfferStage(String offerStage) {
		SetText(OfferStage, offerStage);
	}

	public void ClickOnSavePosition() {
		clickandwait(SavePosition);
	}

	public void clickOnOkButtonOnPopup() {
		clickandwait(OKButtonOnPopup);
	}

	public void waitforPositionTableToAppear() {
		waitforelementtoBecomeVisible(FirstTableRow);
	}

	public void ClickOnNextButton() {
		clickandwait(NextButton);

	}

}
