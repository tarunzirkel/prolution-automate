package com.Prolution.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class RFQPage4 extends BasePage {

	public RFQPage4(ThreadLocal<WebDriver> driver) {
		super(driver);
		//  Auto- constructor stub
	}
	
	
	By Sales=By.xpath("//select[@id='COMPANY_department_Vertrieb']//following-sibling::span//input");
	
	By InternalSales=By.xpath("//select[@id='COMPANY_department_Vertiebsinnendienst']//following-sibling::span//input");
	
	By Department=By.xpath("//select[@id='DepartmentAL']");
	
	By TeamExternalDepartment=By.xpath("//select[@id='DepartmentExtAL']");
	
	By EmployeeDropDown=By.xpath("//*[@id=\"DepartmentTeamAL\"]/option");
	
	By TeamExternalEmployeeDropDown=By.xpath("//select[@id='DepartmentTeamExtAL']//option");

	By SaveTeam=By.xpath("//button[@id='btndone']");
	
	By FinishButton=By.xpath("//button[text()='Finish']");
	
	
	public void EnterValueinSales(String value)
	{
		SetText(Sales, value);
		PressEnter(Sales);
	}
	
	
	public void EnterValueinInternalSales(String value)
	{
		SetText(InternalSales, value);
		PressEnter(InternalSales);
	}
	
//	public void ChooseTheDepartment(String Departments) 
//	{
//		
//		SelectFromDropDownUsingNameOfOption(Department, Departments);
//	}
	
//	public void ChooseTheExternalDepartment(String Departments) 
//	{
//		waitforelementtoBecomeVisible(EmployeeDropDown);
//		SelectFromDropDownUsingNameOfOption(TeamExternalDepartment, Departments);
//		waitforelementtoBecomeVisible(TeamExternalEmployeeDropDown);
//	}
	
	public void ClickOnSavePosition()
	{
		clickandwait(SaveTeam);
		waitTillAlertPresent();
//		PressEnter();
//		try {
//		Thread.sleep(5000);
//		}
//	catch (Exception e) {
//		// TODO: handle exception
//		System.out.println(e);
//	}	
	
	
	
	}
	
	public void ClickOnFinishButton()
	{
    	clickandwait(FinishButton);
        javascriptButtonClick(FinishButton);
//		Actions act= new Actions(driver.get());
//		act.click(driver.get().findElement(FinishButton)).build().perform();
//		

	
	
	
	
	}
	
	
	
	
	
	
	
	
	
	
		
	
}
