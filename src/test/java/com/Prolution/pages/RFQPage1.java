package com.Prolution.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RFQPage1 extends BasePage {

	public RFQPage1(ThreadLocal<WebDriver> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	By CustomerDropDown = By.xpath("//span[@class='selection']/span[1]");

	By DesiredCustomer = By.xpath("//ul[@id='select2-ORGUNIT-results']//li[2]");

	By RFQName = By.id("PROJECT_NAME");

	By CustomerOfferDate = By.id("OFFER_DATE");

	By UnitDropdown = By.xpath("//select[@id='Unit']//following-sibling::div/button[@title='None selected']");

	By UnitDropdownCheckBox = By.xpath("//select[@id='Unit']//following-sibling::div/ul//li[1]"); // For AL
	By UnitDropdownCheckBoxForIron = By.xpath("//select[@id='Unit']//following-sibling::div/ul//li[3]"); // For FE
	
	By StatusExternalDropDown = By.id("STATUS_EXTERNAL");

	By NextButton = By.id("btnNext");

	By EntryDate = By.xpath("//input[@name='ENTRY_DATE']");

	By CustomerRequestID = By.xpath("//input[@id='CREQUEST_ID']");

	By Remarks = By.xpath("//textarea[@id='DESCRIPTION']");

	By OptionalField1 = By.xpath("//input[@id='COLUMN1']");

	By OptionalField2 = By.xpath("//input[@id='COLUMN2']");

	public void SelectCustomer(String Customer) {

		clickandwait(CustomerDropDown);
		waitforelementtoBecomeVisible(DesiredCustomer);
		clickandwait(DesiredCustomer);
	}

	public void EnterRFQName(String RFQName) {
		SetText(this.RFQName, RFQName);
	}

	public void EnterOfferDate(String Date) {
		SetText(CustomerOfferDate, Date);
	}

	public void SelectTwoUnits(String IndexOfUnit, String IndexofUnit) {
		By x = By.xpath("//select[@id='Unit']//following-sibling::div/ul//li[" + IndexOfUnit + "]");
		By xy = By.xpath("//select[@id='Unit']//following-sibling::div/ul//li[" + IndexofUnit + "]");
		By xx=By.xpath("//*[@id=\"ProjectForm\"]/div[1]/div[1]/div[2]/div/span/span/div/button");              //Address of DropDown With select aluminium
		clickandwait(UnitDropdown);
	    Clickandwait(x);
	    clickandwait(xx);
	    clickandwait(xy);

	}

	public void SelectUnit() {
		// For Aluminium Selection
		clickandwait(UnitDropdown);
		waitforelementtoBecomeVisible(UnitDropdownCheckBox);
		clickandwait(UnitDropdownCheckBox);
	}

	

	public void SelectStatusExternal(String Status) {
		SelectFromDropDown(StatusExternalDropDown, Status);

	}

	public void SetEntryDate(String Date) {
		SetText(EntryDate, Date);

	}

	public void EnterCustomerID(String CustomerRequestId) {
		SetText(CustomerRequestID, CustomerRequestId);
	}

	public void EnterRemarks(String Remark) {
		SetText(Remarks, Remark);
	}

	public void EnterOptionalField1(String OptionnalFieldOne) {
		SetText(OptionalField1, OptionnalFieldOne);
	}

	public void EnternOptionalField2(String OptionalFieldTwo) {
		SetText(OptionalField2, OptionalFieldTwo);
	}

	public void ClickOnNextButton() {
		clickandwait(NextButton);
	}

}
