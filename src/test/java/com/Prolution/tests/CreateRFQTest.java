package com.Prolution.tests;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import Extras.ReportGenerator;
import utils.TestUtils;

@Listeners(Extras.Listeneres.class)

public class CreateRFQTest extends BaseTest {

	@Test
	public void CreateRFQWithExistingCustomer() throws Exception {

		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully navigated to the url.");

		pages.getLoginPage().getURL(prop.URL());

		ReportGenerator.getExtentReport().log(Status.INFO, "set email and password.");
		pages.getLoginPage().SetEmailAndPassword(prop.UserName(), prop.PWD());

		ReportGenerator.getExtentReport().log(Status.INFO, "Clicked On Login Button.");
		pages.getLoginPage().ClickOnLoginButton();

		ReportGenerator.getExtentReport().log(Status.INFO, "Moved To RFQ and Clicked On Create RFQ.");
		pages.getLoginPage().MoveToRFQAndClick();

		ReportGenerator.getExtentReport().log(Status.INFO, "Selected User from Customer Dropdown.");
		pages.getCreateRFQPage1().SelectCustomer("Test Automation");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter RFQ Name");
		pages.getCreateRFQPage1().EnterRFQName("sample request");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Offer Date");
		pages.getCreateRFQPage1().EnterOfferDate("30.10.2022");

		ReportGenerator.getExtentReport().log(Status.INFO, "Select Unit From Dropdown");
		pages.getCreateRFQPage1().SelectUnit();

		ReportGenerator.getExtentReport().log(Status.INFO, "Select Status External From Dropdown");
		pages.getCreateRFQPage1().SelectStatusExternal("0");

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Next Button");
		pages.getCreateRFQPage1().ClickOnNextButton();
		
//		CreateRFQWithExistingCustomer
//--------------------------------------------------------------------------------------------------------------------------------->
		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Part Name Field");
		pages.getRFQPage2().EnterPartNameField("sample test part name");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Sample Drawing Number");
		pages.getRFQPage2().EnterDrawingNumberRawCastingField("sample drawing number");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Drawing Number ZSB1");
		pages.getRFQPage2().EnterDrawingNumberZSB1("sample drawing number 2");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Volume Piece Per Year");
		pages.getRFQPage2().EnterVolume_pieces_perYear("23");

		ReportGenerator.getExtentReport().log(Status.INFO, "Type Of Request Select");
		pages.getRFQPage2().SelectTypeOfRequest(1);

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Save Position");
		pages.getRFQPage2().ClickOnSavePosition();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On OK Button When Popup Will appear");
		pages.getRFQPage2().clickOnOkButtonOnPopup();

		ReportGenerator.getExtentReport().log(Status.INFO, "Wait For Appear Table in Bottom");
		pages.getRFQPage2().waitforPositionTableToAppear();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Next Button");
		pages.getRFQPage2().ClickOnNextButton();
		
//		CreateRFQWithExistingCustomer
//-------------------------------------------------------------------------------------------------------------------------------->
		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Folder");
		pages.getRFQPage3().ClickOnTheFolders();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Next Button");
		pages.getRFQPage3().ClickOnNextButton();
		
//		CreateRFQWithExistingCustomer
//----------------------------------------------------------------------------------------------------------------------------------->
		ReportGenerator.getExtentReport().log(Status.INFO, "Input The Value in Internal Sales");
		pages.getRFQPage4().EnterValueinInternalSales("Demo User");

		ReportGenerator.getExtentReport().log(Status.INFO, "Input The Value in sales");
		pages.getRFQPage4().EnterValueinSales("Sven Koch");

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Save Position Button");
		pages.getRFQPage4().ClickOnSavePosition();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Finish Button");
		pages.getRFQPage4().ClickOnFinishButton();

	}
	
//================================================================================================================================================================>
	

	@Test(invocationCount = 5)
	public void CreateRFQWithNewCustomer() throws Exception {

		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully navigated to the url.");
		pages.getLoginPage().getURL(prop.URL());

		ReportGenerator.getExtentReport().log(Status.INFO, "set email and password.");
		pages.getLoginPage().SetEmailAndPassword(prop.UserName(), prop.PWD());

		ReportGenerator.getExtentReport().log(Status.INFO, "Clicked On Login Button.");
		pages.getLoginPage().ClickOnLoginButton();

		ReportGenerator.getExtentReport().log(Status.INFO, "Moved To RFQ and Clicked On Create RFQ.");
		pages.getLoginPage().MoveToRFQAndClick();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On New Customer Button");
		pages.getRFQPageNewCustomer().ClickOnNewCustomerButton();

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Company Name");
		pages.getRFQPageNewCustomer().EnterCompanyName("Test");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Location");
		pages.getRFQPageNewCustomer().EnterLocation("India");

		ReportGenerator.getExtentReport().log(Status.INFO, "Select The Country");
		pages.getRFQPageNewCustomer().SelectCountry();

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The SAP-ID");
		pages.getRFQPageNewCustomer().EnterSAPId("ABCDE");

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Customer Checked Button");
		pages.getRFQPageNewCustomer().ClickOnCheckBox();

	}
	
	
//================================================================================================================================================================>	

	@Test
	public void CreateRFQWithAllFields() throws Exception {
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully navigated to the url.");
		pages.getLoginPage().getURL(prop.URL());

		ReportGenerator.getExtentReport().log(Status.INFO, "set email and password.");
		pages.getLoginPage().SetEmailAndPassword(prop.UserName(), prop.PWD());

		ReportGenerator.getExtentReport().log(Status.INFO, "Clicked On Login Button.");
		pages.getLoginPage().ClickOnLoginButton();

		ReportGenerator.getExtentReport().log(Status.INFO, "Moved To RFQ and Clicked On Create RFQ.");
		pages.getLoginPage().MoveToRFQAndClick();

		ReportGenerator.getExtentReport().log(Status.INFO, "Selected User from Customer Dropdown.");
		pages.getCreateRFQPage1().SelectCustomer("Customer TC1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter RFQ Name");
		pages.getCreateRFQPage1().EnterRFQName("Test RFQ");

//		ReportGenerator.getExtentReport().log(Status.INFO, "Enter the Entry Date");                //BY DEFAULT ENTRY DATE IS TODAY DATE
//		pages.getCreateRFQPage1().SetEntryDate("24.02.2022");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Offer Date");
		pages.getCreateRFQPage1().EnterOfferDate("28.03.2022");

		ReportGenerator.getExtentReport().log(Status.INFO, "Select Unit From Dropdown And Here Select Al(Aluminium)");
		pages.getCreateRFQPage1().SelectUnit();

		ReportGenerator.getExtentReport().log(Status.INFO, "Select Status External From Dropdown");
		pages.getCreateRFQPage1().SelectStatusExternal("0");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter the Customer ID");
		pages.getCreateRFQPage1().EnterCustomerID("Cust Req1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter the Remarks");
		pages.getCreateRFQPage1().EnterRemarks("This is a test remark");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter the Optional Filed One");
		pages.getCreateRFQPage1().EnterOptionalField1("OPTIONAL FIELD ONE IS ");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter the Optional Filed Two");
		pages.getCreateRFQPage1().EnternOptionalField2("Optional Field Two Is ");

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Next Button");
		pages.getCreateRFQPage1().ClickOnNextButton();
		
//		CreateRFQWithAllFields
//------------------------------------------------------------------------------------------------------------------------------------>
		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Part Name Field");
		pages.getRFQPage2().EnterPartNameField("FE 1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Requested Part Button");
		pages.getRFQPage2().SelectCheck();

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Part Number Finished Part");
		pages.getRFQPage2().EnterTheValueOfPartNumberFinishedPart("a001b");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Drawing Number Raw Casting");
		pages.getRFQPage2().EnterDrawingNumberRawCastingField("sample drawing number");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Drawing Number ZSB1");
		pages.getRFQPage2().EnterDrawingNumberZSB1("sample drawing number 2");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Value Of Drawing Index Finish Part");
		pages.getRFQPage2().EnterTheDrawingIndexFinishedPart("test Drawing Index");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The date Of Drawing Date Raw casting");
		pages.getRFQPage2().EnterTheDrawingDateRawCasting("20.02.2021");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The date Of Drawing Date ZSB");
		pages.getRFQPage2().EnterTheDrawingDateZSB("25.03.2021");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Finish Part Weight in kg");
		pages.getRFQPage2().EnterTheFinishedPartWeight("500.50");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Reference Id");
		pages.getRFQPage2().EnterTheReferenceId("Test123");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Machine");
		pages.getRFQPage2().EnterTheMachine();

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Plant Section");
		pages.getRFQPage2().EnterThePlantSelection();

		ReportGenerator.getExtentReport().log(Status.INFO, "Choose The Type Of Request");
		pages.getRFQPage2().SelectTypeOfRequest(3);

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Part Number Raw Casting");
		pages.getRFQPage2().EnterThePartNumberRawCasting("Test_Part");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The ZSB Number");
		pages.getRFQPage2().EnterTheZSB_Nr("Test ZSB Number");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Drawing number Finished Part");
		pages.getRFQPage2().EnterTheDrawingnumberFinishedPart("Test Drawing No.123");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Drawing Index Raw Casting");
		pages.getRFQPage2().EnterTheDrawingIndexRawCasting("Test Index 1234");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Drawing Index ZSB");
		pages.getRFQPage2().EnterTheDrawingIndexZSB("Test ZSB 1234");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Drawing Date Finished Part");
		pages.getRFQPage2().EnterTheDrawingDateFinishedPart("25.02.2022");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Raw Casting Weight in kg");
		pages.getRFQPage2().EnterTheRawCastingWeight("550");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The SAP_MATERIAL Nr.");
		pages.getRFQPage2().EnterTheSAP_MaterialNr("Test1023");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Material Alloy");
		pages.getRFQPage2().EnterTheMaterialAlloy();

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Volume Piece PerYear");
		pages.getRFQPage2().EnterVolume_pieces_perYear("25");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Remarks");
		pages.getRFQPage2().EnterTheRemarks("Test Remark");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Life time Model Equipment [Parts]");
		pages.getRFQPage2().EnterTheLifetimeModelEquipmentParts("Test Equipments");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Life time Core Box [Parts]");
		pages.getRFQPage2().EnterTheLifetimeCoreBoxParts("Test Core Box Part");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Lifetime Stamping Tool[Parts]");
		pages.getRFQPage2().EnterTheLifetimeStampingToolParts("Test Stamping Parts");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Data Status Finished Part");
		pages.getRFQPage2().EnterTheDataStatusFinishedPart("Test Finished Part Status");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Calculated Price [�]");
		pages.getRFQPage2().EnterTheCalculatedPrice("1000");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Lifetime Insert Matrix [Parts]");
		pages.getRFQPage2().EnterTheLifeTimeInsertPart("Test Insert Part");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Lifetime Die Casting Tool [Parts]");
		pages.getRFQPage2().EnterTheValueOfLifetimeDieCastingToolParts("Test Die cast Tool");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Data Status Raw Casting");
		pages.getRFQPage2().EnterTheDataStatusRawCasting("Test Raw casting");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Offer Stage");
		pages.getRFQPage2().EnterTheOfferStage("25000");

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Save Position");
		pages.getRFQPage2().ClickOnSavePosition();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On OK Button When Popup Will appear");
		pages.getRFQPage2().clickOnOkButtonOnPopup();

		ReportGenerator.getExtentReport().log(Status.INFO, "Wait For Appear Table in Bottom");
		pages.getRFQPage2().waitforPositionTableToAppear();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Next Button");
		pages.getRFQPage2().ClickOnNextButton();
		
//		CreateRFQWithAllFields
//------------------------------------------------------------------------------------------------------------------------------------------------>
		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Folder");
		pages.getRFQPage3().ClickOnTheFolders();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On SubFolder");
		pages.getRFQPage3().ClickOnSubFolder();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Child Folder Of SubFolder");
		pages.getRFQPage3().ClickOnChildFolderOfSubFolder();

		ReportGenerator.getExtentReport().log(Status.INFO, "Select A Files");
		pages.getRFQPage3().SelectFile();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Upload Button To Upload a File");
		pages.getRFQPage3().ClickOnUploadButton();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Next Button");
		pages.getRFQPage3().ClickOnNextButton();
		
//		CreateRFQWithAllFields
//-------------------------------------------------------------------------------------------------------------------------------------------------------->
		ReportGenerator.getExtentReport().log(Status.INFO, "Input The Value in Internal Sales");
		pages.getRFQPage4().EnterValueinInternalSales("Demo User");

		ReportGenerator.getExtentReport().log(Status.INFO, "Input The Value in sales");
		pages.getRFQPage4().EnterValueinSales("Sven Koch");

//		ReportGenerator.getExtentReport().log(Status.INFO, "Choose The Department");
//		pages.getRFQPage4().ChooseTheDepartment("aa4a82be-fbf5-469b-8ea3-e55b8c94d77a");

//		ReportGenerator.getExtentReport().log(Status.INFO, "Choose The External Department");
//		pages.getRFQPage4().ChooseTheExternalDepartment("9085c33b-2517-4620-a49c-bf1737ccb1ea");

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Save Position Button");
		pages.getRFQPage4().ClickOnSavePosition();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Finish Button");
		pages.getRFQPage4().ClickOnFinishButton();
	}
	
//================================================================================================================================================================>	

	@Test
	public void CreateRFQWithTwoUnits() throws Exception {
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully navigated to the url.");
		pages.getLoginPage().getURL(prop.URL());

		ReportGenerator.getExtentReport().log(Status.INFO, "set email and password.");
		pages.getLoginPage().SetEmailAndPassword(prop.UserName(), prop.PWD());

		ReportGenerator.getExtentReport().log(Status.INFO, "Clicked On Login Button.");
		pages.getLoginPage().ClickOnLoginButton();

		ReportGenerator.getExtentReport().log(Status.INFO, "Moved To RFQ and Clicked On Create RFQ.");
		pages.getLoginPage().MoveToRFQAndClick();

		ReportGenerator.getExtentReport().log(Status.INFO, "Selected User from Customer Dropdown.");
		pages.getCreateRFQPage1().SelectCustomer("Customer TC1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter RFQ Name");
		pages.getCreateRFQPage1().EnterRFQName("Test RFQ");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Offer Date");
		pages.getCreateRFQPage1().EnterOfferDate("28.03.2022");

		ReportGenerator.getExtentReport().log(Status.INFO, "Select Two Units From Dropdown And Here Select Fe(Iron)");
		pages.getCreateRFQPage1().SelectTwoUnits("1", "3");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter the Customer ID");
		pages.getCreateRFQPage1().EnterCustomerID("Cust Req1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Select Status External From Dropdown");
		pages.getCreateRFQPage1().SelectStatusExternal("0");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter the Remarks");
		pages.getCreateRFQPage1().EnterRemarks("This is a test remark");

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Next Button to Navigate On Position Page");
		pages.getCreateRFQPage1().ClickOnNextButton();
		
		
		
//	    CreateRFQWithTwoUnits
//------------------------------------------------------------------------------------------------------------------------------->	
//       Position Page Select Unit Iron Here (SELECT TWO UNITS )
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Select Radio Button On Position page");
		pages.getRFQPage2().ToSelectRadioButton();

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Part Name Field");
		pages.getRFQPage2().EnterPartNameField("FE 1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Part Number Raw Casting");
		pages.getRFQPage2().EnterThePartNumberRawCasting("FE1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Part Number Finished Part");
		pages.getRFQPage2().EnterTheValueOfPartNumberFinishedPart("FE1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Volume Piece PerYear");
		pages.getRFQPage2().EnterVolume_pieces_perYear("500");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Remarks");
		pages.getRFQPage2().EnterTheRemarks("Test Remark");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Drawing Number Raw Casting");
		pages.getRFQPage2().EnterDrawingNumberRawCastingField("FE1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Drawing number Finished Part");
		pages.getRFQPage2().EnterTheDrawingnumberFinishedPart("FE1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Drawing Index Raw Casting");
		pages.getRFQPage2().EnterTheDrawingIndexRawCasting("FE1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Value Of Drawing Index Finish Part");
		pages.getRFQPage2().EnterTheDrawingIndexFinishedPart("test Drawing Index");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The date Of Drawing Date Raw casting");
		pages.getRFQPage2().EnterTheDrawingDateRawCasting("20.02.2021");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Drawing Date Finished Part");
		pages.getRFQPage2().EnterTheDrawingDateFinishedPart("25.02.2022");

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Requested Part Button");
		pages.getRFQPage2().SelectCheck();

		ReportGenerator.getExtentReport().log(Status.INFO, "Choose The Type Of Request");
		pages.getRFQPage2().SelectTypeOfRequest(3);

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Raw Casting Weight in kg");
		pages.getRFQPage2().EnterTheRawCastingWeight("550");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Finish Part Weight in kg");
		pages.getRFQPage2().EnterTheFinishedPartWeight("500.50");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Plant Section");
		pages.getRFQPage2().EnterThePlantSelection();

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Nests/Cavities");
		pages.getRFQPage2().EnterTheNests("20");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The SAP_MATERIAL Nr.");
		pages.getRFQPage2().EnterTheSAP_MaterialNr("FE1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Reference Id");
		pages.getRFQPage2().EnterTheReferenceId("FE1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Material Alloy");
		pages.getRFQPage2().EnterTheMaterialAlloy();

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Machine");
		pages.getRFQPage2().EnterTheMachine();

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Drawing Number ZSB1");
		pages.getRFQPage2().EnterDrawingNumberZSB1("FE1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Save Position");
		pages.getRFQPage2().ClickOnSavePosition();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On OK Button When Popup Will appear");
		pages.getRFQPage2().clickOnOkButtonOnPopup();

		ReportGenerator.getExtentReport().log(Status.INFO, "Wait For Appear Table in Bottom");
		pages.getRFQPage2().waitforPositionTableToAppear();
		
		
//		CreateRFQWithTwoUnits
//------------------------------------------------------------------------------------------------------------------------->
//		Position Page Select Aluminium (SELECT TWO UNITS)
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Select Radio Button On Position page");
		pages.getRFQPage2().ToSelectRadioButtonAluminium();

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Part Name Field");
		pages.getRFQPage2().EnterPartNameField("Al1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Part Number Raw Casting");
		pages.getRFQPage2().EnterThePartNumberRawCasting("AL1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Part Number Finished Part");
		pages.getRFQPage2().EnterTheValueOfPartNumberFinishedPart("AL1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Volume Piece PerYear");
		pages.getRFQPage2().EnterVolume_pieces_perYear("500");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Remarks");
		pages.getRFQPage2().EnterTheRemarks("Test Remark");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Drawing Number Raw Casting");
		pages.getRFQPage2().EnterDrawingNumberRawCastingField("AL1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Drawing number Finished Part");
		pages.getRFQPage2().EnterTheDrawingnumberFinishedPart("AL1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Drawing Index Raw Casting");
		pages.getRFQPage2().EnterTheDrawingIndexRawCasting("Al1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Value Of Drawing Index Finish Part");
		pages.getRFQPage2().EnterTheDrawingIndexFinishedPart("AL1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The date Of Drawing Date Raw casting");
		pages.getRFQPage2().EnterTheDrawingDateRawCasting("20.02.2021");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Drawing Date Finished Part");
		pages.getRFQPage2().EnterTheDrawingDateFinishedPart("25.02.2022");

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Requested Part Button");
		pages.getRFQPage2().SelectCheck();

		ReportGenerator.getExtentReport().log(Status.INFO, "Choose The Type Of Request");
		pages.getRFQPage2().SelectTypeOfRequest(3);

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Raw Casting Weight in kg");
		pages.getRFQPage2().EnterTheRawCastingWeight("550");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Finish Part Weight in kg");
		pages.getRFQPage2().EnterTheFinishedPartWeight("500.50");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Plant Section");
		pages.getRFQPage2().EnterThePlantSelection();

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Nests/Cavities");
		pages.getRFQPage2().EnterTheNests("20");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The SAP_MATERIAL Nr.");
		pages.getRFQPage2().EnterTheSAP_MaterialNr("AL1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Reference Id");
		pages.getRFQPage2().EnterTheReferenceId("Al1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Material Alloy");
		pages.getRFQPage2().EnterTheMaterialAlloy();

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter The Machine");
		pages.getRFQPage2().EnterTheMachine();

		ReportGenerator.getExtentReport().log(Status.INFO, "Enter Drawing Number ZSB1");
		pages.getRFQPage2().EnterDrawingNumberZSB1("Al1");

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Save Position");
		pages.getRFQPage2().ClickOnSavePosition();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On OK Button When Popup Will appear");
		pages.getRFQPage2().clickOnOkButtonOnPopup();

		ReportGenerator.getExtentReport().log(Status.INFO, "Wait For Appear Table in Bottom");
		pages.getRFQPage2().waitforPositionTableToAppear();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Next Button");
		pages.getRFQPage2().ClickOnNextButton();
		
//		CreateRFQWithTwoUnits
//------------------------------------------------------------------------------------------------------------------------->
//		For Aluminium File upload in folder
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Top Most Folder");
		pages.getRFQPage3().ClickOnTheFolders();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On SubFolder");
		pages.getRFQPage3().ClickOnSubFolder();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Child Folder Of SubFolder (Anfrageunterlagen)");
		pages.getRFQPage3().ClickOnChildFolderOfSubFolder();

		ReportGenerator.getExtentReport().log(Status.INFO, "Select A Files type .pdf");
		pages.getRFQPage3().SelectFile();

		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Upload Button To Upload a File");
		pages.getRFQPage3().ClickOnUploadButton();
		
		
//		CreateRFQWithTwoUnits
//------------------------------------------------------------------------------------------------------------------------->
//		For Iron File upload in folder
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Click On radio Button To Select Another Material If selected two");
		pages.getRFQPage3().ClickOnRadioButton();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Top Most Folder");
		pages.getRFQPage3().ClickOnTheFolders();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Click On SubFolder (KundendatenFolder)");
		pages.getRFQPage3().ClickOnSubFolder();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Child Folder Of SubFolder (Zeichnungen)");
		pages.getRFQPage3().ClickOnChildFolderOfSubFolders();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Select A Files File Type .doc");
		pages.getRFQPage3().SelectFiles();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Upload Button To Upload a File");
		pages.getRFQPage3().ClickOnUploadButton();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Click On Next Button to Move On Next Page");
		pages.getRFQPage3().ClickOnNextButton();

	}

}
