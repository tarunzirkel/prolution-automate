package com.Prolution.tests;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import Extras.ReportGenerator;
import utils.TestUtils;



@Listeners(Extras.Listeneres.class)
public class CustomerTest extends BaseTest {

	
String FirstName=TestUtils.uniqueTextGenerator("Ashraf");
	
	String LastName=TestUtils.uniqueTextGenerator("Test_Engg");
	
	String BuisnessName=TestUtils.uniqueTextGenerator("Test_Automation");
	
	String Email=TestUtils.getUniqueEmail();
	
	@Test
	public void CreateCustomer() throws Exception
	{
	//	System.out.println("------------------------------------------------------------------------------------------------------------>");
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully navigated to the url.");
		pages.getLoginPage().getURL(prop.URL());

	    
		ReportGenerator.getExtentReport().log(Status.INFO, "set email and password.");
		pages.getLoginPage().SetEmailAndPassword(prop.UserName(), prop.PWD());

		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Login Button.");
		pages.getLoginPage().ClickOnLoginButton();
//
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Customer tab.");
		pages.getCustomersPage().clickOnCustomersButton();
//
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Add Customer Button.");
		pages.getCustomersPage().ClickOnAddCustomersButton();

		ReportGenerator.getExtentReport().log(Status.INFO, "Adding First Name.");
		pages.getNewCustomerPage().SetFirstName(FirstName);

		ReportGenerator.getExtentReport().log(Status.INFO, "Adding Last Name.");
		pages.getNewCustomerPage().SetLastName(LastName);

		ReportGenerator.getExtentReport().log(Status.INFO, "Adding Buisness Name.");
		pages.getNewCustomerPage().SetBuisnessName(BuisnessName);

		ReportGenerator.getExtentReport().log(Status.INFO, "Adding Email Name.");
		pages.getNewCustomerPage().SetEmail(Email);

		ReportGenerator.getExtentReport().log(Status.INFO, "Selecting Phone options");
		pages.getNewCustomerPage().SelectPhoneOption();

		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Customer tab.");
		pages.getNewCustomerPage().ClickOnCreateCustomerbutton();

		ReportGenerator.getExtentReport().log(Status.INFO, "Verifying the Success Message for customer creation.");
		Assert.assertTrue(pages.getCustomersPage().verifySucessMessage(), "Success message is not displayed");

	}
	
	
	
	
	@Test(dependsOnMethods = "CreateCustomer")
	public void EditCreatedCustomer() throws Exception
	{
		System.out.println("------------------------------------------------------------------------------------------------------------>"+prop.addcustomer_successmsg());
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully navigated to the url.");
		pages.getLoginPage().getURL(prop.URL());

	    
		ReportGenerator.getExtentReport().log(Status.INFO, "set email and password.");
		pages.getLoginPage().SetEmailAndPassword(prop.UserName(), prop.PWD());

		
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Login Button.");
		pages.getLoginPage().ClickOnLoginButton();
//
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Customer tab.");
		pages.getCustomersPage().clickOnCustomersButton();
	
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Searching for the Email "+Email);
		pages.getCustomersPage().SearchForCustomer(Email);
		
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Edit button.");
		pages.getCustomersPage().ClickOnEditButton();
	
	
		ReportGenerator.getExtentReport().log(Status.INFO, "Changing Email from "+Email);
		
		Email=TestUtils.getUniqueEmail();
		
		ReportGenerator.getExtentReport().log(Status.INFO, " to "+Email);
		pages.getNewCustomerPage().SetEmail(Email);

		
		
	
		ReportGenerator.getExtentReport().log(Status.INFO, "clicking on save changes button.");
		pages.getNewCustomerPage().ClickOnSaveChangesButton();
		
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Verifying the Success Message for Update.");
		Assert.assertTrue(pages.getCustomersPage().verifySucessMessage(), "Success message is not displayed");


		
	}
	
	
	
	
	
	
	
	
	
	
	@Test(dependsOnMethods = "EditCreatedCustomer")
	public void DeleteCreatedCustomer() throws Exception
	{
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully navigated to the url.");
		pages.getLoginPage().getURL(prop.URL());

	    
		ReportGenerator.getExtentReport().log(Status.INFO, "set email and password.");
		pages.getLoginPage().SetEmailAndPassword(prop.UserName(), prop.PWD());

		
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Login Button.");
		pages.getLoginPage().ClickOnLoginButton();
//
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Customer's Button.");
		pages.getCustomersPage().clickOnCustomersButton();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Searching for the Email "+Email);
		pages.getCustomersPage().SearchForCustomer(Email);
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully clicked on Action Button.");
		pages.getInvoicesPage().ClickOnActionButton();
		
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Selecting the Delete option from the Action dropdown.");
		pages.getCustomersPage().SelectAnOptionFromActionsDropdownForCustomer("Delete");
		
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully handled the alert.");
		pages.getCustomersPage().handleAlert();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully verified the success message.");
		Assert.assertTrue(pages.getCustomersPage().verifySucessMessage(), "Success message is not displayed");
	
	}
	
	
	
	
	
}
