package com.Prolution.tests;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.Prolution.pages.PageCollection;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import Extras.Listeneres;
import io.github.bonigarcia.wdm.WebDriverManager;
import utils.PropertiesConfig;

public class BaseTest {

   protected PropertiesConfig prop;
	BaseTest()
	{
	prop=ConfigFactory.create(PropertiesConfig.class);
	
	}
	
	//static WebDriver dr;
	public static ThreadLocal<WebDriver>driver=new ThreadLocal<WebDriver>();
	
	public PageCollection pages;
	


	//public  WebDriver Currentdriver;
	
	
	
	public ExtentTest test;
	
	//public  String pathToChromeDriver=System.getProperty("user.dir")+"\\Resources\\chromedriver.exe";
	
	
	@BeforeMethod
	public void SetUp() throws MalformedURLException
	{
		System.out.println("In setup, you have choosed to run the system as "+prop.Execution());
		 if(Objects.isNull(driver.get())) {
			 if(prop.Execution().trim()=="Remote") {
		 URL url=new URL("http://localhost:4444/wd/hub");
		 DesiredCapabilities des=DesiredCapabilities.chrome();
		 driver.set(new RemoteWebDriver(url,des));
		 
		 driver.get().manage().window().maximize();
		 
		
		 }
	
	
	else  {
		 
		 System.out.println("In local setup");
		 WebDriverManager.chromedriver().setup();
	ChromeOptions options;

	options=new ChromeOptions();
	options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
	//options.setHeadless(false);
	driver.set(new ChromeDriver(options));	
	System.out.println(driver.get());
	driver.get().manage().window().maximize();
	
	
			 }
			 }
}
	

	@BeforeMethod(alwaysRun = true)
	public void initialie()
	{
		System.out.println("Initialized page collection");
		pages=new PageCollection(driver);
		
	}
	
	
	
	
	@AfterMethod
	public void TearDown(ITestResult res)
	{
		System.out.println("inside teardown");
		
		if(Objects.nonNull(driver.get()))
		{
			driver.get().close();
			driver.get().quit();
			driver.remove();
	}
		
		
	
		
		
		
		}
	
	
	
	
	public static String TakeScreenshot(String TestName,WebDriver driver) throws IOException
	{
		TakesScreenshot scrShot =((TakesScreenshot)driver);
		
		File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
		
		File DestFile=new File(System.getProperty("user.dir") + "\\Report\\"+TestName+".png");
		System.out.println("-------------------------------------------------------------------->>>"+DestFile);
		FileUtils.copyFile(SrcFile, DestFile);
		
		return System.getProperty("user.dir") + "\\Report\\"+TestName+".png";
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}