package com.Prolution.tests;

import static org.testng.Assert.assertFalse;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.Status;

import Extras.Listeneres;
import Extras.ReportGenerator;
import utils.TestUtils;


@Listeners(Extras.Listeneres.class)
public class LoginTest extends BaseTest {

	String FirstName = TestUtils.uniqueTextGenerator("Ashraf");

	String LastName = TestUtils.uniqueTextGenerator("Test_Engg");

	String BuisnessName = TestUtils.uniqueTextGenerator("Test_Automation");

	String Email = TestUtils.getUniqueEmail();
	
	

	@Test
	public void CreateCustomer() throws Exception {
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully navigated to the url.");
		pages.getLoginPage().getURL("https://abhiabc.repairshopr.com/users/sign_in");

	    
		ReportGenerator.getExtentReport().log(Status.INFO, "set email and password.");
		pages.getLoginPage().SetEmailAndPassword("teststackshare@gmail.com", "Repairshop@12345");

		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Login Button.");
		pages.getLoginPage().ClickOnLoginButton();
//
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Customer tab.");
		pages.getCustomersPage().clickOnCustomersButton();
//
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Add Customer Button.");
		pages.getCustomersPage().ClickOnAddCustomersButton();

		ReportGenerator.getExtentReport().log(Status.INFO, "Adding First Name.");
		pages.getNewCustomerPage().SetFirstName(FirstName);

		ReportGenerator.getExtentReport().log(Status.INFO, "Adding Last Name.");
		pages.getNewCustomerPage().SetLastName(LastName);

		ReportGenerator.getExtentReport().log(Status.INFO, "Adding Buisness Name.");
		pages.getNewCustomerPage().SetBuisnessName(BuisnessName);

		ReportGenerator.getExtentReport().log(Status.INFO, "Adding Email Name.");
		pages.getNewCustomerPage().SetEmail(Email);

		ReportGenerator.getExtentReport().log(Status.INFO, "Selecting Phone options");
		pages.getNewCustomerPage().SelectPhoneOption();

		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Customer tab.");
		pages.getNewCustomerPage().ClickOnCreateCustomerbutton();

		ReportGenerator.getExtentReport().log(Status.INFO, "Verifying the Success Message for customer creation.");
		Assert.assertTrue(pages.getCustomersPage().verifySucessMessage(), "Success message is not displayed");

	}

	@Test
	public void CreateCustomer2() throws Exception {
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully navigated to the url.");
		pages.getLoginPage().getURL("https://abhiabc.repairshopr.com/users/sign_in");

	    
		ReportGenerator.getExtentReport().log(Status.INFO, "set email and password.");
		pages.getLoginPage().SetEmailAndPassword("teststackshare@gmail.com", "Repairshop@12345");

		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Login Button.");
		pages.getLoginPage().ClickOnLoginButton();
//
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Customer tab.");
		pages.getCustomersPage().clickOnCustomersButton();
//
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Add Customer Button.");
		pages.getCustomersPage().ClickOnAddCustomersButton();

		ReportGenerator.getExtentReport().log(Status.INFO, "Adding First Name.");
		pages.getNewCustomerPage().SetFirstName(FirstName);

		ReportGenerator.getExtentReport().log(Status.INFO, "Adding Last Name.");
		pages.getNewCustomerPage().SetLastName(LastName);

		ReportGenerator.getExtentReport().log(Status.INFO, "Adding Buisness Name.");
		pages.getNewCustomerPage().SetBuisnessName(BuisnessName);

		ReportGenerator.getExtentReport().log(Status.INFO, "Adding Email Name.");
		pages.getNewCustomerPage().SetEmail(Email);

		ReportGenerator.getExtentReport().log(Status.INFO, "Selecting Phone options");
		pages.getNewCustomerPage().SelectPhoneOption();

		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Customer tab.");
		pages.getNewCustomerPage().ClickOnCreateCustomerbutton();

		ReportGenerator.getExtentReport().log(Status.INFO, "Verifying the Success Message for customer creation.");
		Assert.assertTrue(pages.getCustomersPage().verifySucessMessage(), "Success message is not displayed");

	}


}
