package com.Prolution.tests;


import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.Prolution.pages.HomePage.More;
import com.aventstack.extentreports.Status;

import Extras.Listeneres;
import Extras.MainMenuItem;
import Extras.ReportGenerator;


@Listeners(Extras.Listeneres.class)
public class InvoicesTest extends BaseTest{
	
	
	String NameOfCustomer="Customer For Invoice Testing";
	
	int Generated_Invoice_Id=0;
	
	
	
	@Test(priority=1)
	public void GenerateBillingNumberForExistingCustomer() throws Exception {
		

		
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully navigated to the url.");
		pages.getLoginPage().getURL(prop.URL());

	    
		ReportGenerator.getExtentReport().log(Status.INFO, "set email and password.");
		pages.getLoginPage().SetEmailAndPassword(prop.UserName(), prop.PWD());

		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Login Button.");
		pages.getLoginPage().ClickOnLoginButton();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicked On More Button");
		pages.getHomePage().ClickOnAMainMenuItem(MainMenuItem.More);
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Invoices Button.");
		pages.getHomePage().ClickOnItemUnderMainButton(More.Invoices);
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Create New Invoice Button.");
		pages.getInvoicesPage().ClickOnCreateNewInvoiceButton();
		
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Entered Name of existing customer.");
		pages.getInvoicesPage().SetCustomerNameOnInvoiceField(NameOfCustomer);
		
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Selected the first customer name from the dropdown");
		pages.getInvoicesPage().SelectFirstCustomerNameFromCustomerDropdown();
		
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Create Invoice Button.");
		pages.getInvoicesPage().ClickOnCreateInvoiceButton();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "fetching the invoive id");
		Generated_Invoice_Id=Integer.parseInt(pages.getInvoicesPage().getInvoiceNumberFromBillingPage().replaceAll("[^0-9?!\\.]", ""));
		ReportGenerator.getExtentReport().log(Status.INFO, "Succesfully fetched the Invoice id = "+Generated_Invoice_Id);
	}
	
	
	@Test(priority=2,dependsOnMethods = "GenerateBillingNumberForExistingCustomer")
	public void ValidateGeneratedBillingNumberForExistingCustomer() throws Exception {
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully navigated to the url.");
		pages.getLoginPage().getURL(prop.URL());

	    
		ReportGenerator.getExtentReport().log(Status.INFO, "set email and password.");
		pages.getLoginPage().SetEmailAndPassword(prop.UserName(), prop.PWD());

		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Login Button.");
		pages.getLoginPage().ClickOnLoginButton();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicked On More Button");
		pages.getHomePage().ClickOnAMainMenuItem(MainMenuItem.More);
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Invoices Button.");
		pages.getHomePage().ClickOnItemUnderMainButton(More.Invoices);
	
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully verified the generated invoice id on the invoice table.");
		Assert.assertTrue(Integer.parseInt(pages.getInvoicesPage().GetFirstInvoiceIdOnCustomerTable())==Generated_Invoice_Id,"Generated Invoice Id is not found on Invoice Page.");
		
	}
	
	
	
	
	
	@Test(priority=2,dependsOnMethods = "ValidateGeneratedBillingNumberForExistingCustomer")
	public void EditGeneratedInvoice() throws Exception {
		

		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully navigated to the url.");
		pages.getLoginPage().getURL(prop.URL());

	    
		ReportGenerator.getExtentReport().log(Status.INFO, "set email and password.");
		pages.getLoginPage().SetEmailAndPassword(prop.UserName(), prop.PWD());

		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Login Button.");
		pages.getLoginPage().ClickOnLoginButton();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicked On More Button");
		pages.getHomePage().ClickOnAMainMenuItem(MainMenuItem.More);
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully searched for the Invoice number "+Generated_Invoice_Id);
		pages.getInvoicesPage().SearchInvoice(Integer.toString(Generated_Invoice_Id));
		
		
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On first invoice available on the table");
		pages.getInvoicesPage().ClickOnFirstInvoiceIdOnCustomerTable();
		
		
		
		pages.getCustomersPage().ClickOnEditButton();
		
		pages.getInvoicesPage().clickOnTookPaymentCheckbox("check");
		
		pages.getInvoicesPage().clickOnTodayDate();
		
		pages.getInvoicesPage().ClickOnUpdateInvoiceButton();
		
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	@Test(priority=2,dependsOnMethods = "EditGeneratedInvoice")
	public void DeleteGeneratedEstimateNumberForExistingCustomer() throws Exception {
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully navigated to the url.");
		pages.getLoginPage().getURL("https://abhiabc.repairshopr.com/users/sign_in");

	    
		ReportGenerator.getExtentReport().log(Status.INFO, "set email and password.");
		pages.getLoginPage().SetEmailAndPassword("teststackshare@gmail.com", "Repairshop@12345");

		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Login Button.");
		pages.getLoginPage().ClickOnLoginButton();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On Invoices Button.");
		pages.getInvoicesPage().ClickOnInvoicesButton();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully searched for the Invoice number "+Generated_Invoice_Id);
		pages.getInvoicesPage().SearchInvoice(Integer.toString(Generated_Invoice_Id));
		
		
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Clicking On first invoice available on the table");
		pages.getInvoicesPage().ClickOnFirstInvoiceIdOnCustomerTable();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "clicking on the Delete button from action dropdown");
		pages.getInvoicesPage().SelectAnOptionFromActions("Delete");
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully handle alert.");
		pages.getInvoicesPage().handleAlert();
		
		ReportGenerator.getExtentReport().log(Status.INFO, "Successfully verified the delete message.");
		Assert.assertTrue(pages.getInvoicesPage().VerifyDeleteSuccessMessage(),"Delete message is not visible.");
		
	}
	
	
	
	
	
	
	
	
	
	

}
