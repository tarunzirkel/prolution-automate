package Extras;

public enum MainMenuItem {

	Backup,
	Training,
	DashBoard,
	Chat,
	Customers,
	AssestsRMM,
	Scripts,
	More;
}
